const { main } = require('./src/main');

main.init({ mmi: 99469, prevIndex: 0 });

const howMany = 0xFFFF;

const start = Date.now();
let last;

for (let i = 0; i < howMany; i++) {
    if (i % 100000) {
        console.log('Speed:', ((Date.now() - start) / i) * 1000, 'HEXes a second');
    }

    last = main.getMoreRandomValue();
    // last = main.getValueFromSequence();
} 

console.log('total speed:', ((Date.now() - start) / howMany) * 1000, 'HEXes a second')
console.log('last', last);
