# Hex Generator

## Task

We’d like you to write us a program that:
* Generates 8-digit hexadecimal codes to be used for 2FA.

The rules are as follows:
* Every time you run the program, it should emit one 8-digit hexadecimal code;
* It should emit every possible code before repeating;
* It should not print "odd-looking" codes such as 0xAAAAAAAA or 0x01234567 or any commonly used words, phrases, or [Hexspeak](https://en.wikipedia.org/wiki/Hexspeak) such as 0xDEADBEEF;
* Codes should be emitted in apparently random order.

## My story

I found two solutions for this task:

### The first approach - a table
1. Generate all numbers
2. remove all 'bad-numbers' and hex speak
3. save in DB
4. Sort using Fisher–Yates shuffle 
5. Go one by one and return values
6. When we reached the last one - reset the index and go on step 4.

Pluses:
1. It's quite secure because the values are sorted +- randomly
2. Easy to stop and continue getting the same sequence if we store the current index id in DB
3. Fast access and return from the sorted table

Minuses:
1. Long init generation the table with only correct HEXes
2. Long and unexpected resorting of the table when the index is out of the date
3. Lots's of additional memory to save DB
4. Additional complexity of the app because additional DB only for generating


### The second approach - an algorithm

Pluses: 
1. No additional memory
2. Speed is +- the same for each get (110-140 HEXes a sec)
3. No additional complexity and risks because of a DB
4. Can be stopped and continued using a prev index and MMI value from a last generated value.

Minuses:
1. Not secure, it's possible to crack it if get some sequence of HEXes. Can be improved by a buffer and random access it
2. Relatively random values can be also improved by a buffer and random access it
3. Slower than 1st approach in the best case, because we calc and check a number each time.

---

So I selected second just because it's more interesting to make it and needs some additional knowledge and investigations to do it. I described both of them to show that I know about edge cases of both options, and in my opinion 1st idea is better because of security. But I've chosen second because of a cool challenge and experience. And I solved it using "multiplicative inverse technique", which I found on StackOverflow.



## How to detect hex-speak?

I just created a dictionary with words and I use shifts, XOR, and AND to check it. You can find the solution in `hexSpeakDetect.js` and `hexSpeakDetect.test.js` files.

## How to detect odd-numbers?

I check the sequence of 3 numbers one by one, and the difference between them. If it's 0 or negative or positive and repeats twice, then it means that there is 0x000, 0x123, or 0x321 sequence of digits. Again shifts, AND, XOR. The solution is in: `oddNumberDetect.js`, `oddNumberDetect.test.js` files.




## Resources

1. https://stackoverflow.com/questions/35102073/generating-a-random-non-repeating-sequence-of-all-integers-in-net
2. https://en.wikipedia.org/wiki/Linear_congruential_generator
3. https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
4. https://www.mathsisfun.com/numbers/coprime-calculator.html
5. https://crypto.stackexchange.com/questions/87220/cryptographically-secure-linear-congruential-generator-is-it-possible
6. https://en.wikipedia.org/wiki/Next-bit_test
7. https://dirask.com/posts/JavaScript-own-random-number-generator-custom-implementation-9pYYRp
8. https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
9. https://www.rapidtables.com/convert/number/decimal-to-hex.html
10. https://en.wikipedia.org/wiki/Hexspeak
