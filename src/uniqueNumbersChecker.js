class UniqueNumbersChecker {
    constructor() {
        this.maxNumber = 2**32 - 1;
        this.numbersCount = this.maxNumber + 1;

        this.reset();
    }

    reset() {
        // allocate 2*32 bits to save each number. One byte - 3 bits
        this.buffer = Buffer.alloc(Math.ceil(this.numbersCount / 3));
        this.unusedCellsCounter = this.numbersCount;
    }

    __getByteAndBitPosition(value) {
        return {
            bytePosition: Math.trunc(value / 3),
            bitPosition: value % 3,
        };
    }

    checkIsInArray(value) {
        const { bitPosition, bytePosition } = this.__getByteAndBitPosition(value);

        const byte = this.buffer.readUInt8(bytePosition);


        let result = false;
        let write;

        if (bitPosition === 0) {
            result = byte & 0b001;
            write = byte | 0b001;
        } else if (bitPosition === 1) {
            result = byte & 0b010;
            write = byte | 0b010;
        } else {
            result = byte & 0b100;
            write = byte | 0b100;
        }

        result = !!result;

        if (!result) {
            this.buffer.writeUInt8(write, bytePosition);
            this.unusedCellsCounter--;
        }

        return result;
    }
}

const uniqueNumbersChecker = new UniqueNumbersChecker();

module.exports = { uniqueNumbersChecker };
