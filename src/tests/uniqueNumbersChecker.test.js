const { uniqueNumbersChecker } = require('../uniqueNumbersChecker');

// TODO: add more tests
describe('uniqueNumbersChecker', () => {
    it('check right ranges', () => {
        expect(0xFFFFFFFF).toBe(uniqueNumbersChecker.maxNumber);
        expect(0xFFFFFFFF + 1).toBe(uniqueNumbersChecker.numbersCount);
    })

    it('__getByteAndBitPosition should works correctly', () => {
        // 000 001
        expect(uniqueNumbersChecker.__getByteAndBitPosition(0)).toEqual({
            bytePosition: 0,
            bitPosition: 0,
        });

        // 000 010
        expect(uniqueNumbersChecker.__getByteAndBitPosition(1)).toEqual({
            bytePosition: 0,
            bitPosition: 1,
        });

        // 000 100
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2)).toEqual({
            bytePosition: 0,
            bitPosition: 2,
        });

        // 001 000
        expect(uniqueNumbersChecker.__getByteAndBitPosition(3)).toEqual({
            bytePosition: 1,
            bitPosition: 0,
        });

        // 010 000
        expect(uniqueNumbersChecker.__getByteAndBitPosition(4)).toEqual({
            bytePosition: 1,
            bitPosition: 1,
        });

        // 010 000 000 000
        expect(uniqueNumbersChecker.__getByteAndBitPosition(10)).toEqual({
            bytePosition: 3,
            bitPosition: 1,
        });

        // 010 000 ...
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2**31-1)).toEqual({
            bytePosition: 715827882,
            bitPosition: 1,
        });

        // 001 000 ...
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2**31-2)).toEqual({
            bytePosition: 715827882,
            bitPosition: 0,
        });

        // 000 010 ...
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2**31-4)).toEqual({
            bytePosition: 715827881,
            bitPosition: 1,
        });

        // 001 000 ...
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2**32-1)).toEqual({
            bytePosition: 1431655765,
            bitPosition: 0,
        });

        // 000 010 ...
        expect(uniqueNumbersChecker.__getByteAndBitPosition(2**32-4)).toEqual({
            bytePosition: 1431655764,
            bitPosition: 0,
        });
    });

    it('checkIsInArray', () => {
        const valuesToCheck = [0,1,2,3,4,5,2**32 - 1, 2**32 - 2, 2**32 - 3, 2**32 - 4, 2**32 - 5];

        valuesToCheck.map((value) => {
            expect(uniqueNumbersChecker.checkIsInArray(value)).toBe(false);
            expect(uniqueNumbersChecker.checkIsInArray(value)).toBe(true);
            expect(uniqueNumbersChecker.checkIsInArray(value)).toBe(true);
            expect(uniqueNumbersChecker.checkIsInArray(value)).toBe(true);
        });

        uniqueNumbersChecker.reset();

        valuesToCheck.map((value) => {
            expect(uniqueNumbersChecker.checkIsInArray(value)).toBe(false);
        });
        
    })
});
