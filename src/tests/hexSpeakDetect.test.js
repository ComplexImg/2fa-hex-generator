const { isNumberInHexSpeakDictionary } = require('../hexSpeakDetect');

// TODO: add more tests
describe('hexSpeakDetect', () => {
    it('check words from dictionary with different position', () => {
        const checkCases = [
            0x1234BEEF,
            0x123BEEF4,
            0x12BEEF34,
            0x1BEEF234,
            0xBEEF1234,
            0xD15EA5E,
            0xBAD,
            0xFABAD123,
        ];
    
        checkCases.map((check) => {
            expect(isNumberInHexSpeakDictionary(check)).toBe(true);
        });
    });

    it('check a word not from dictionary', () => {
        expect(isNumberInHexSpeakDictionary(0x12345678)).toBe(false);
        expect(isNumberInHexSpeakDictionary(0x123BEEE4)).toBe(false);
    });
});
