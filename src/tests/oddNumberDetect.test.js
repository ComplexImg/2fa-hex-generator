const { isNumberOdd } = require('../oddNumberDetect');

// TODO: add more tests
describe('oddNumberDetect', () => {
    it('check 3+ same digits one by one', () => {
        const checkCases = [
            0x00004192,
            0xF0004192,
            0xF3334192,
            0xF0422290,
            0xD0422222,
            0xBAAAD087,
        ];
    
        checkCases.map((check) => {
            expect(isNumberOdd(check)).toBe(true);
        });
    });

    it('check 3+ increasing/decreasing digits one by one with different diff', () => {
        const checkCases = [
            0x01234567,
            0x08312394,
            0xF339F932,
            0x987FA900,
            0xABE,
            0x123BEEF4,
            0xBEEF1230,
            0x035CBA00,
        ];
    
        checkCases.map((check) => {
            expect(isNumberOdd(check)).toBe(true);
        });
    });

    it('check 2 increasing/decreasing digits and the rest wrong cases', () => {
        const checkCases = [
            0xABABABAB,
            0xAB7AB7AB,
            0x1429830A,
        ];
    
        checkCases.map((check) => {
            expect(isNumberOdd(check)).toBe(false);
        });
    });

});
