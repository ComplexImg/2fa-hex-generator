const MAX_SAME_DIFF_LENGTH = 2; // 3 in order, 2 pairs next to each other

const isNumberOdd = (value) => {
    const digits = new Array(8).fill(0);

    let index = 0;

    while (value) {
        digits[index] = value & 0xF;

        index++;

        value >>>= 4;
    }

    let prevDigit = digits[digits.length - 2];
    let prevDiff = digits[digits.length - 1] - digits[digits.length - 2];
    let sameDiffCounter = 1;

    for (let i = digits.length - 3; i >= 0; i--) {
        const currentDiff = prevDigit - digits[i];

        if (currentDiff === prevDiff) {
            sameDiffCounter++;

            if (sameDiffCounter >= MAX_SAME_DIFF_LENGTH) {
                return true;
            }
        } else {
            prevDiff = currentDiff;
            sameDiffCounter = 1;
        }

        prevDigit = digits[i];
    }

    return false;
};

module.exports = { isNumberOdd };
