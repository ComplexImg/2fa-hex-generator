// Sieve of Eratosthenes
const findPrimes = (num = 10) => {
    const numArr = (new Array(num + 1)).fill(true);
    numArr[0] = numArr[1] = false;

    for (let i = 2; i <= Math.sqrt(num); i++) {
        for (let j = 2; i * j <= num; j++){
            numArr[i * j] = false;
        }
    }

    const result = [];

    for (let i = 0; i < numArr.length; i++) {
        if (numArr[i]) {
            result.push(i);
        }
    }

    return result;
 };

const randomFromRange = (min = 0, max = 0xFFFFFFFE) => Math.floor(Math.random() * (max - min + 1) + min);

module.exports = { findPrimes, randomFromRange };
