// I store number of bits to check because
// the dict wont be huge and to save some performance
// and don't calc it each time, it's faster to store.
const hexSpeakDictionary = [
    // mask, number of bits to check
    [0xBEEF, 0xFFFF],
    [0xDEAD, 0xFFFF],
    [0xB10C, 0xFFFF],
    [0xBAD, 0xFFF],
    [0x4B1D, 0xFFFF],
    [0xF00D, 0xFFFF],
    [0xBABE, 0xFFFF],
    [0xB105, 0xFFFF],
    [0xB16, 0xFFF],
    [0xB00B, 0xFFFF],
    [0xCAFE, 0xFFFF],
    [0xB0BA, 0xFFFF],
    [0xD06, 0xFFF],
    [0xFACE, 0xFFFF],
    [0xFEED, 0xFFFF],
    [0xD15EA5E, 0xFFFFFF],
    [0xBEAF, 0xFFFF],
    [0xD00D, 0xFFFF],
    [0xFA11, 0xFFFF],
    [0x10CC, 0xFFFF],
    [0xDEFEC8ED, 0xFFFFFFFF],
    [0xF1AC, 0xFFFF],
    [0xb00c, 0xFFFF],
    [0xFEE1, 0xFFFF],
    [0xC0DE, 0xFFFF],
    [0xCE11, 0xFFFF],
    [0xF001, 0xFFFF],
    [0xC0D3, 0xFFFF],
];

// number of letters - min word length
const SHIFTS_NUMBER = 8 - 3;

// perf: 0(n) - dict size
const isNumberInHexSpeakDictionary = (value) => {
    for (let i = 0; i < SHIFTS_NUMBER; i++) {
        // console.log('value', value, parseInt(value, 10).toString(16));

        for (let wordIndex = 0; wordIndex < hexSpeakDictionary.length; wordIndex++) {
            const [checkWord, lastBitsNumber] = hexSpeakDictionary[wordIndex];

            // ((a ^ b) & 0x1F) == 0)
            if (((value ^ checkWord) & lastBitsNumber) === 0) {
                // console.log('res', value, checkWord, parseInt(value, 10).toString(16), parseInt(checkWord, 10).toString(16), '/', parseInt(value, 10).toString(2), parseInt(checkWord, 10).toString(2) );
                return true;
            } 
        }

        value >>>= 4;
    }

    return false;
};

module.exports = { isNumberInHexSpeakDictionary };
