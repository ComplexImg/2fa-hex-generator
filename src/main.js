const { isNumberOdd } = require('./oddNumberDetect');
const { isNumberInHexSpeakDictionary } = require('./hexSpeakDetect');
const { randomFromRange } = require('./utils');
const mmis = require('./mmis.json');
const { mmiGenerator } = require('./mmiGenerator');

class Main {
    init({ prevIndex, mmi, randomBufferSize = 1000 }) {
        this.mmi = !mmi || mmi < 0 ? mmis[randomFromRange(0, mmis.length - 1)] : mmi;

        this.randomBufferSize = randomBufferSize;
        this.randomBuffer;
        this.randomBufferFilled = false;

        this.generator = mmiGenerator({ prevIndex, mmi });
    }

    __resetGenerator() {
        while (true) {
            const newMmi = mmis[randomFromRange(0, mmis.length - 1)];

            if (this.mmi !== newMmi) {
                this.mmi = newMmi;
                this.randomBufferFilled = false;
                this.generator = mmiGenerator({ prevIndex: 0, mmi: newMmi });
            }
        }
    }

    __getNextValue() {
        while (true) {
            const { done, value } = this.generator.next();

            if (!value && done) {
                this.__resetGenerator();
            } else {
                const val = value.value;

                if (!isNumberOdd(val) && !isNumberInHexSpeakDictionary(val)) {
                    return value;
                }

                // console.log('next loop', value, !isNumberOdd(val), isNumberInHexSpeakDictionary(val));
            }
        }
    }

    // can be used after stop
    getValueFromSequence() {
        return this.__getNextValue();
    }

    // can't use if was stoped
    // TODO: case when generator whas finished we should return all array values! and only after reinit it
    getMoreRandomValue() {
        if (!this.randomBufferFilled) {
            this.randomBuffer = new Array(this.randomBufferSize);
            for (let i = 0; i < this.randomBufferSize; i++) {
                this.randomBuffer[i] = this.__getNextValue();
            }
            this.randomBufferFilled = true;
        }

        const randomIndex = randomFromRange(0, this.randomBufferSize - 1);
        const value = this.__getNextValue();
        const result = this.randomBuffer[randomIndex];
        this.randomBuffer[randomIndex] = value;

        return result;
    }
}

const main = new Main();

module.exports = { main };
