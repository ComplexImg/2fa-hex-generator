const { uniqueNumbersChecker } = require('./uniqueNumbersChecker');
const mmis = require('./mmis.json');

// TODO: some save stuff like checks input
// https://en.wikipedia.org/wiki/Modular_multiplicative_inverse
function* mmiGenerator({ prevIndex, mmi }) {
    prevIndex = prevIndex ?? 0;
 
    const totalCapacity = 0xFFFFFFFF + 1;

    let index = prevIndex + 1;

    const onePercent = Math.trunc(0xFFFFFFFF / 100);

    uniqueNumbersChecker.reset();

    console.log('!mmiGenerator', { index, prevIndex, mmi });

    while (index < totalCapacity + 1) {   
        const value = ((index * mmi) % totalCapacity);
    
        if (index % onePercent === 0) {
            console.log(index / 0xFFFFFFFF);
        }
    
        if (uniqueNumbersChecker.checkIsInArray(value)) {
            throw new Error('Duplicate!!!');
        };

        yield { value, index, mmi };
    
        index++;
    }
};

module.exports = { mmiGenerator };
